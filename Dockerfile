FROM registry.gitlab.com/hackerhappyhour/iot/docker-node-pigpio:latest

COPY . /usr/src/app

WORKDIR /usr/src/app

CMD ["node", "index.js"]
