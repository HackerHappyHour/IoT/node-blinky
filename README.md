# node-blinky

Currently tested on `raspbian-stretch` desktop, Raspberry Pi 3, docker version `18.06.x`.

This is an example of how to use the `registry.gitlab.com/hackerhappyhour/iot/docker-node-pigpio` image
to dockerize your IoT node.js app on Raspberry Pi.

This example is taken directly from the README of the `pigpio` npm package (see [github.com/fivdi/pigpio](https://github.com/fivdi/pigpio)),
and executed in the HHH `docker-node-pigpio` image.

To run this, you'll need to have your Rasperry Pi wired up exactly like the image below, and then run:

    docker run -it --rm --privileged registry.gitlab.com/hackerhappyhour/iot/node-blinky:latest

![Rasperry Pi Wiring Diagram](https://raw.githubusercontent.com/fivdi/pigpio/master/example/led-button.png)
